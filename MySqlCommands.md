Ingresar a un contenedor

* docker ps --> Muestra informacion de todos los contenedores.

* docker exec -i -t CONTAINER ID /bin/bash 

Salier de un contenedor

* exit

Contenedor MySQL

Referencia Curso de MySql: https://www.youtube.com/watch?v=hBvF4q0gVLE&list=PLpKurRfiInIzevhtEvXqv-GerRiKkEAjv

* mysql -u root -p root --> Conectar a la DB. Referencia: https://www.youtube.com/watch?v=L2p3xOKUQW0

* show databases; --> Muestra las bases de datos existentes.

* create database Infractions; --> Crea la base de datos. Referencia: https://www.youtube.com/watch?v=L2p3xOKUQW0

* use Infractions; --> Este comando nos permite selecionar la DB en la que vamos a trabajar.

* create table Nombre_De_La_Tabla; --> Crea una tabla.

* show tables; --> Muesta las tablas creadas en nuestra base de datos.

Ejemplo: (Crea la tabla Import_Infraciones)

 CREATE TABLE IF NOT EXISTS `import_infracciones` (
   `acta` varchar(7) NOT NULL,
   `fecha_acta` varchar(8) NOT NULL,
   `acta_transito` varchar(3) NOT NULL,
   `lugar_infraccion` varchar(35) NOT NULL,
   `hora_infraccion` varchar(8) NOT NULL,
   `nro_cuasa` varchar(9) NOT NULL,
   `dni` varchar(9) NOT NULL,
   `nombre` varchar(35) NOT NULL,
   `tipo_documento` varchar(1) NOT NULL,
   `domicilio` varchar(25) NOT NULL,
   `nro_domicilio` varchar(5) NOT NULL,
   `localidad` varchar(20) NOT NULL,
   `cod_postal` varchar(4) NOT NULL,
   `desc_vehiculo` varchar(15) NOT NULL,
   `dominio` varchar(12) NOT NULL,
   `desc_falta` varchar(140) NOT NULL,
   `estado_causa` varchar(20) NOT NULL,
   `codif_estado_cauda` varchar(3) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 * describe import_infracciones; --> Muesta los campos y tipo de dtos de una tabla.

 * load data local infile ' path completo donde se encuenra el archivo.txt ' into table TABLE_NAME fields terminated by '\r\n';
    --> Inserta los datos desde un archovo TXT delimitado por ','

    Ejemplo de contenido TXT:
                             Tere, 39, 1982-11-25, F

    Referencia: https://www.youtube.com/watch?v=ADDGLd623-Q                      

 * Comando super cheto, para hacerlo de una desde el proyecto!!!!

  docker exec -i aa87264df1d9 mysql -uroot -proot Infractions < import_infracciones.sql  // esto es para cargar la bd AL MYSQL DE  docker

